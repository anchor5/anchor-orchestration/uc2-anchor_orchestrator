import connexion
from flask import Response
import logging

import swagger_server.logic.input_mgmt as inmgmt
import swagger_server.logic.simultation_mgmt as simmgmt
import swagger_server.logic.monitoring_mgmt as monmgmt

from swagger_server.models.start_simulation_input import StartSimulationInput  # noqa: E501
from swagger_server.models.update_simulation_input import UpdateSimulationInput  # noqa: E501


def start_ran_simulation(body):  # noqa: E501
    """Endpoint to request the start of a new RAN simulation.

     # noqa: E501

    :param body: The payload of the request must embed all the fields of the files to be configured as input for the ANChOR simulator. The input of the first round of the ANChOR simulator is given by 4 files:
- TLE (satellite orbital parameters) files [e.g. on slides: &quot;TLE_Nanosat_1&quot; file]
- UE positioning (coordinates of the fixed sensors UEs) [e.g. on slides: &quot;Coordinates_4_UEs&quot; file]
- Network Configuration [e.g. on slides: &quot;networkConfiguration_0&quot; file]
- Resource Configuration [e.g. on slides: &quot;resourceConfiguration_0&quot; file]
    :type body: dict | bytes

    :rtype: None
    """
    
    if connexion.request.is_json:
        
        logging.info("Received start RAN simulation request with the following payload:\n")
        request_body = connexion.request.get_json() ## dict
        logging.info(request_body)
        
        logging.info("Start processing the request payload in order to produce the RAN simulator input files.")
        inmgmt.produce_start_simulation_input_files(StartSimulationInput.from_dict(request_body)) # noqa: E501
        logging.info("Written all the input files required to start a RAN simulation locally.")
        
        logging.info("Start writing remotely on the RAN simulator machine the RAN simulator input files.")
        written_all_files = inmgmt.write_remotely_start_simulation_files()
        if written_all_files == False:
            return Response(
                "Cannot write remotely configuration files.", status=500)
        logging.info("Written all the input files required to start a RAN simulation remotely.")
        
        ## ensure that no errors will be raisen afterwards
        if "metrics-to-monitor" not in request_body.keys():
            request_body['metrics-to-monitor'] = ""
        if "topic-name" not in request_body.keys():
            request_body['topic-name']=""
        logging.info("Configuring the monitoring platform in order to star monitoring the requested metrics")
        monitoring_platform_configuration_response = monmgmt.configure_monitoring(request_body['metrics-to-monitor'], request_body['topic-name'])
        if monitoring_platform_configuration_response.status_code == 400:
            return Response(
                "Cannot configure monitoring platform, returned error: <" + monitoring_platform_configuration_response.text + ">", status=400)
        logging.info("Monitoring Platform configured correctly")

        logging.info("Starting the remote simulation process")
        simmgmt.start_simulation()
        logging.info("Simulation started")
        
    return Response("Simulation Started Correctly", status=200)


def update_simulation_config(body):  # noqa: E501
    """Update the input configuration to the RAN simulator in order to start a new round of the simulation.

     # noqa: E501

    :param body: The request body should contain the overall simulation configuration. 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        logging.info("Received update RAN simulation request with the following payload:\n")
        request_body = connexion.request.get_json()
        logging.info(request_body)

        logging.info("Start processing the request payload in order to produce the RAN simulator input files to be updated.")
        update_simulation_input = UpdateSimulationInput.from_dict(request_body)  # noqa: E501
        inmgmt.produce_update_simulation_input_files(update_simulation_input)
        logging.info("Written all the input files required to update a RAN simulation locally.")
        
        logging.info("Start writing remotely on the RAN simulator machine the RAN simulator input files.")
        written_all_files = inmgmt.write_remotely_update_simulation_files()
        if written_all_files == False:
            return Response(
                "Cannot write remotely configuration files.", status=500)
        logging.info("Written all the input files required to update a RAN simulation remotely.")
        logging.info("Updating the simulation process")
        simmgmt.update_simulation()
        logging.info("Simulation updated")

    return Response("Simulation Updated Correctly", status=200)

