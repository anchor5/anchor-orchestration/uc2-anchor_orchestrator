# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.start_simulation_input import StartSimulationInput  # noqa: E501
from swagger_server.models.update_simulation_input import UpdateSimulationInput  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_start_ran_simulation(self):
        """Test case for start_ran_simulation

        Endpoint to request the start of a new RAN simulation.
        """
        body = StartSimulationInput()
        response = self.client.open(
            '/simulation/start',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_simulation_config(self):
        """Test case for update_simulation_config

        Update the input configuration to the RAN simulator in order to start a new round of the simulation.
        """
        body = UpdateSimulationInput()
        response = self.client.open(
            '/simulation/update',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
