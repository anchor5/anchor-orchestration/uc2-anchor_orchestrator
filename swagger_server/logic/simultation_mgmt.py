import logging 
import os
import swagger_server.logic.utils as utils

round_counter = 0

#/home/ubuntu/ns-allinone-3.36/ns-3.36/ns3 run "ran-simulator --simRoundIndex=0" --no-build
def start_simulation():
    try:
        utils.round_counter = 0
        cmd = "ssh -i {} {}@{} \'{} run \"{} --simRoundIndex={}\" --no-build\'".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_ns3_path'],
            utils.configuration['REMOTE_PATHS']['remote_ran_simulator_path'],
            utils.round_counter)
        logging.debug("Executing update command: {}".format(cmd))
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))

        logging.info("Simulation started, executed the command:\n{}".format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False


def update_simulation():
    try:
        utils.round_counter += 1
        cmd = "ssh -i {} {}@{} \'{} run \"{} --simRoundIndex={}\" --no-build\'".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_ns3_path'],
            utils.configuration['REMOTE_PATHS']['remote_ran_simulator_path'],
            utils.round_counter)
    
        logging.debug("Executing update command: {}".format(cmd))
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))

        logging.info("Simulation updated, executed the command:\n{}".format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False
