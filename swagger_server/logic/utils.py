import toml
import logging

def init_configuration():
    global configuration
    global round_counter
    try:
        configuration = toml.load("configuration/ANChOR_Orchestrator.conf")
        logging.info("Configuration correctly loaded")
        round_counter = configuration["APP"]["initial_value_round_counter"]
    except Exception as e:
        logging.error("Error reading the configuration file: {}".format(e))


def produce_text_from_list(list):
    line = ""
    for index,element in enumerate(list):
        line += str(element)
        if index != len(list)-1:
            line += " "
        else:
            line += "\n"
    return line
        


def produce_text_from_list_of_lists(list_of_lists):
    list_of_lines = []
    for list_element in list_of_lists:
        line = ""
        for sublist_element_index in range(len(list_element)):
            if sublist_element_index != len(list_element)-1:
                line += "{}\t".format(list_element[sublist_element_index])
            else:
                line += "{}\n".format(list_element[sublist_element_index])
        list_of_lines.append(line)
    return list_of_lines