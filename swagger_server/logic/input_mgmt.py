import logging
import os

import swagger_server.logic.utils as utils


def produce_start_simulation_input_files(simulation_input):
    #
    # produce_tle_files(simulation_input.gnb_list) <<-- current RAN simulator NS3 source code expects it as static files
    #
    # produce_ue_coordinates_file(simulation_input.initial_resource_configuration) <<-- produced as output by the current RAN simulator NS3 source code
    #
    produce_network_configuration_file(len(simulation_input.gnb_list), simulation_input.traffic_profile_cardinality, simulation_input.initial_resource_configuration, simulation_input.afb_list)
    produce_resource_configuration_file(simulation_input.traffic_profile_cardinality, len(simulation_input.gnb_list), simulation_input.initial_resource_configuration)


def produce_update_simulation_input_files(update_simulation_input):
    logging.info("Received update simulation request")
    produce_resource_configuration_file(update_simulation_input.traffic_profile_cardinality, update_simulation_input.gnb_number, update_simulation_input.updated_resource_configuration)
    logging.info("Written all the input files required to update a RAN simulation locally.")


def write_remotely_start_simulation_files():
    try:
        cmd = "scp -i {} {}/{} {}@{}:{}".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['LOCAL_PATHS']['local_network_configuration_dir'], 
            utils.configuration['LOCAL_PATHS']['local_network_configuration_filename'], 
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_network_configuration_dir']
        )
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False

    logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
        utils.configuration['LOCAL_PATHS']['local_network_configuration_filename'], 
        utils.configuration['RANSIM_SERVER']['ransim_user'],
        utils.configuration['RANSIM_SERVER']['ransim_ip'],
        utils.configuration['REMOTE_PATHS']['remote_network_configuration_dir']))

    try:
        cmd = "scp -i {} {}/{} {}@{}:{}".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['LOCAL_PATHS']['local_resource_configuration_dir'], 
            utils.configuration['LOCAL_PATHS']['local_resource_configuration_filename'], 
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_resource_configuration_dir'])
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False
    
    logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
        utils.configuration['LOCAL_PATHS']['local_resource_configuration_filename'], 
        utils.configuration['RANSIM_SERVER']['ransim_user'],
        utils.configuration['RANSIM_SERVER']['ransim_ip'],
        utils.configuration['REMOTE_PATHS']['remote_resource_configuration_dir']))

    # os.listdir returns a list of strings
    ## TLE files are static
    '''
    for file in os.listdir("{}".format(utils.configuration['LOCAL_PATHS']['local_tle_dir'])):

        try:
            cmd = "scp -i {} {}{} {}@{}:{}".format(
                utils.configuration['LOCAL_PATHS']['local_private_key_path'],
                utils.configuration['LOCAL_PATHS']['local_tle_dir'], 
                file, 
                utils.configuration['RANSIM_SERVER']['ransim_user'],
                utils.configuration['RANSIM_SERVER']['ransim_ip'],
                utils.configuration['REMOTE_PATHS']['remote_tle_dir'])
            if os.system(cmd) != 0:
                raise Exception('Cannot Execute command:\n{}'.format(cmd))

            logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
                file, 
                utils.configuration['RANSIM_SERVER']['ransim_user'],
                utils.configuration['RANSIM_SERVER']['ransim_ip'],
                utils.configuration['REMOTE_PATHS']['remote_tle_dir']))
        except:
            logging.error('Error while executing command:\n{}'.format(cmd))
            return False
    '''
    ## uePositioning is produced as output
    '''
    try:
        cmd = "scp -i {} {}{} {}@{}:{}".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['LOCAL_PATHS']['local_ue_coordinates_dir'], 
            utils.configuration['LOCAL_PATHS']['local_ue_coordinates_filename'], 
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_dir']
        )
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False

    logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
        utils.configuration['LOCAL_PATHS']['local_ue_coordinates_filename'], 
        utils.configuration['RANSIM_SERVER']['ransim_user'],
        utils.configuration['RANSIM_SERVER']['ransim_ip'],
        utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_dir']))
    '''
    ## gNBPositioning is produced as output
    '''
    try:
        cmd = "scp -i {} {}{} {}@{}:{}".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['LOCAL_PATHS']['local_gnb_positioning_dir'], 
            utils.configuration['LOCAL_PATHS']['local_gnb_positioning_filename'], 
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_gnb_positioning_dir'])
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False
    
    os.system()
    logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
        utils.configuration['LOCAL_PATHS']['local_gnb_positioning_filename'], 
        utils.configuration['RANSIM_SERVER']['ransim_user'],
        utils.configuration['RANSIM_SERVER']['ransim_ip'],
        utils.configuration['REMOTE_PATHS']['remote_gnb_positioning_dir']))
    '''



def write_remotely_update_simulation_files():
    logging.info("Writing all the input files required to update a RAN simulation remotely on RAN simulator server.")
    
    ## now the new file must be written inside the RANsim vm, this is the command to append
    try:
        cmd = "scp -i {} {}/{} {}@{}:{}".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['LOCAL_PATHS']['local_resource_configuration_dir'], 
            utils.configuration['LOCAL_PATHS']['local_resource_configuration_filename'], 
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_resource_configuration_dir'])
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False
    
    logging.info("File: \'{}\' written remotely at {}@{}:{}".format(
        utils.configuration['LOCAL_PATHS']['local_resource_configuration_filename'], 
        utils.configuration['RANSIM_SERVER']['ransim_user'],
        utils.configuration['RANSIM_SERVER']['ransim_ip'],
        utils.configuration['REMOTE_PATHS']['remote_resource_configuration_dir']))

## TLE files are static
'''
def produce_tle_files(gnb_list):
    logging.info("Producing TLE files content")
    # each element inside the list has type GnbDescriptor
    for gnb in gnb_list:
        with open("{}/TLE_{}.txt".format(utils.configuration['LOCAL_PATHS']['local_tle_dir'],gnb._gnb_name), "w") as text_file:
            text_file.write("1 {}{} {}{}{} {}{} {} {} {} {} {}{}\n2 {} {} {} {} {} {} {}{}{}".format(
                gnb._gnb_tle._satellite_catalog_number,
                gnb._gnb_tle._classification,
                gnb._gnb_tle._international_designator_year,
                gnb._gnb_tle._international_designator_launch_number,
                gnb._gnb_tle._international_designator_piece_of_launch,
                gnb._gnb_tle._epoch_year,
                gnb._gnb_tle._epoch_day_and_fractional,
                gnb._gnb_tle._first_derivative_mean_motion,
                gnb._gnb_tle._second_derivative_mean_motion,
                gnb._gnb_tle._radiation_pressure_coefficient,
                gnb._gnb_tle._ephemeris_type,
                gnb._gnb_tle._element_set_number,
                gnb._gnb_tle._checksum_line_1,
                gnb._gnb_tle._satellite_catalog_number,
                gnb._gnb_tle._inclination,
                gnb._gnb_tle._right_ascension,
                gnb._gnb_tle._eccentricity,
                gnb._gnb_tle._argument_of_perigee,
                gnb._gnb_tle._mean_anomaly,
                gnb._gnb_tle._mean_motion,
                gnb._gnb_tle._revolution_number_at_epoch,
                gnb._gnb_tle._checksum_line_2
            ))
    logging.info("TLE files written locally.")
'''
## uePositions.txt is expected as output
'''
def produce_ue_coordinates_file(initial_resource_configuration):
    logging.info("Producing UE coordinates file content")
    UE_latitudes = []
    UE_longitudes = []
    UE_altitudes =[]
    for ue in initial_resource_configuration:
        UE_latitudes.insert(ue["ue-index"],ue["ue-coordinates"]["latitude"])
        UE_longitudes.insert(ue["ue-index"],ue["ue-coordinates"]["longitude"])
        UE_altitudes.insert(ue["ue-index"],ue["ue-coordinates"]["altitude"])
    with open("{}{}".format(utils.configuration['LOCAL_PATHS']['local_ue_coordinates_dir'], utils.configuration['LOCAL_PATHS']['local_ue_coordinates_filename']), "w") as text_file:
        text_file.write("UEIndex latitude longitude altitude\n")
        for ue_index in range(len(initial_resource_configuration)):
            text_file.write("{} {} {} {}\n".format(ue_index, UE_latitudes[ue_index], UE_longitudes[ue_index], UE_altitudes[ue_index]))
    logging.info("UE coordinates file written locally.")
'''


def produce_network_configuration_file(gnb_list_length, traffic_profile_cardinality, initial_resource_configuration, afb_list):
    logging.info("Producing Network Configuration file content")
    UE_traffic_profiles = []
    central_frequency_bands = []
    bandwidth_afb = []
    num_cc_per_afb = []
    central_frequencies_cc = []
    bandwidth_cc = []
    num_bwp_per_cc = []
    central_frequencies_bwp_per_cc = []
    bandwidth_bwp_per_cc = []
    numerology_bwp_per_cc = []

    # UEPerTrafProfile
    UE_traffic_profiles = [0 for column in range(traffic_profile_cardinality)]
    logging.info("ini resource config")
    logging.info(initial_resource_configuration)

    for ue in initial_resource_configuration:
        UE_traffic_profiles[ue["ue-traffic-profile"]] += 1

    for afb_index in range(len(afb_list)):
        afb = afb_list[afb_index] 
        central_frequency_bands.append(afb.afb_central_frequency)
        bandwidth_afb.append(afb.afb_bandwidth)
        num_cc_per_afb.append(len(afb.afb_cc_list))

        for cc_index in range(len(afb.afb_cc_list)):
            cc = afb.afb_cc_list[cc_index]
            num_bwp_per_cc.append(len(cc.cc_bwp_list))
            bandwidth_cc.append(cc.cc_bandwidth)
            central_frequencies_cc.append(cc.cc_central_frequency)

            for bwp in cc.cc_bwp_list:
                # qui siamo nel bwp list del cc cc_index nel afb afb_index
                central_frequencies_bwp_per_cc.append(bwp.bwp_central_frequency)
                bandwidth_bwp_per_cc.append(bwp.bwp_bandwidth)
                numerology_bwp_per_cc.append(bwp.bwp_numerology)

    with open("{}/{}".format(utils.configuration['LOCAL_PATHS']['local_network_configuration_dir'],utils.configuration['LOCAL_PATHS']['local_network_configuration_filename']), "w") as text_file:
        # write the upper part of the resource configuration file
        text_file.write("numgNBs {}\n".format(gnb_list_length))
        text_file.write("numTrafficProfiles {}\n".format(traffic_profile_cardinality))
        text_file.write("numUEsPerTrafProf {}".format(utils.produce_text_from_list(UE_traffic_profiles)))
        text_file.write("numActiveFrequencyBands {}\n".format(str(len(afb_list))))
        text_file.write("centralFrequencyBands {}".format(utils.produce_text_from_list(central_frequency_bands)))
        text_file.write("bandwidthBands {}".format(utils.produce_text_from_list(bandwidth_afb)))
        text_file.write("numCCsPerBand {}".format(utils.produce_text_from_list(num_cc_per_afb)))
        text_file.write("centralFrequencyCCs {}".format(utils.produce_text_from_list(central_frequencies_cc)))
        text_file.write("bandwidthCCs {}".format(utils.produce_text_from_list(bandwidth_cc)))
        text_file.write("numBWPsPerCC {}".format(utils.produce_text_from_list(num_bwp_per_cc)))
        text_file.write("centralFrequencyBWPs {}".format(utils.produce_text_from_list(central_frequencies_bwp_per_cc)))
        text_file.write("bandwidthBWPs {}".format(utils.produce_text_from_list(bandwidth_bwp_per_cc)))
        text_file.write("numerologyBWPs {}".format(utils.produce_text_from_list(numerology_bwp_per_cc)))

    logging.info("Network Configuration file written.")


def produce_resource_configuration_file(traffic_profile_cardinality, gnb_number, initial_resource_configuration):
    logging.info("Producing Resource configuration file content")

    # traffic profile cardinality added because otherwise we have not this information starting from the resource-configuration data
    # the number of columns is given by the number of traffic profile + 1 (column for gNB index)
    # list of lists initialized with all zeros
    UE_traffic_profile_per_gnb = [[0 for column in range(traffic_profile_cardinality + 1)] for row in range(gnb_number)]

    #list of lists to store the UE index, the UE TF and the connected gNB
    UE_resource_allocation = []

    #first element of each list must be the gNB index
    for gnb_index in range(gnb_number):
        UE_traffic_profile_per_gnb[gnb_index][0] = gnb_index

    # increment the traffic profile counters for each gnb (NOTE attention to the column index)
    for ue in initial_resource_configuration:
        UE_traffic_profile_per_gnb[ue["ue-connected-gnb-index"]][ue["ue-traffic-profile"] +1 ] += 1
        UE_resource_allocation.insert(ue["ue-index"], [ ue["ue-index"], ue["ue-traffic-profile"], ue["ue-connected-gnb-index"] ])
    
    upper_part_lines = utils.produce_text_from_list_of_lists(UE_traffic_profile_per_gnb)
    lower_part_lines = utils.produce_text_from_list_of_lists(UE_resource_allocation)

    with open("{}/{}".format(utils.configuration['LOCAL_PATHS']['local_resource_configuration_dir'],utils.configuration['LOCAL_PATHS']['local_resource_configuration_filename']), "w") as text_file:
        # write the upper part of the resource configuration file
        text_file.write("gNB\tgNBIndex\tNumberAttachedUEPerTrafficProfile\n")
        for line in upper_part_lines:
            text_file.write(line)

        # write the lower part of the resource configuration file
        text_file.write("UE\tUEIndex\tUETrafficProfile\tgNBIndexToAttach\n")
        for line in lower_part_lines:
            text_file.write(line)
    
    logging.info("Resource configuration file written.")