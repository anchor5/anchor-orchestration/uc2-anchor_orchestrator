import logging
import requests
import os

import swagger_server.logic.utils as utils

def configure_monitoring(metrics, topic_name):
    
    ## this is the path on which the Telegraf Ransim performs the inotifywait command, so this must be the LAST file written by the RAN simulator
    ## NOTE: in order to make the inotify command run properly the file must exist
    ## the inotifywait cannot be invoked on the directory containing the outputs otherwise the plugin is invoked 3 times (once per file written inside it)

    ## ensure the output file exitence
    logging.debug("Ensuring output file existence.")
    try:
        cmd = "ssh -i {} {}@{} \'touch {}/{}\'".format(
            utils.configuration['LOCAL_PATHS']['local_private_key_path'],
            utils.configuration['RANSIM_SERVER']['ransim_user'],
            utils.configuration['RANSIM_SERVER']['ransim_ip'],
            utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_dir'],
            utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_filename'])
        if os.system(cmd) != 0:
            raise Exception('Cannot Execute command:\n{}'.format(cmd))
    except:
        logging.error('Error while executing command:\n{}'.format(cmd))
        return False
    
    remote_output_file_path = utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_filename']

    remote_network_configuration_filepath = utils.configuration['REMOTE_PATHS']['remote_network_configuration_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_network_configuration_filename']
    remote_resource_configuration_filepath = utils.configuration['REMOTE_PATHS']['remote_resource_configuration_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_resource_configuration_filename']
    remote_ue_coordinates_filepath = utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_ue_coordinates_filename']
    remote_gnb_positioning_filepath = utils.configuration['REMOTE_PATHS']['remote_gnb_positioning_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_gnb_positioning_filename']
    remote_network_status_filepath = utils.configuration['REMOTE_PATHS']['remote_network_status_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_network_status_filename']
    remote_performance_results_filepath = utils.configuration['REMOTE_PATHS']['remote_performance_results_dir'] + "/" + utils.configuration['REMOTE_PATHS']['remote_performance_results_filename']

    remote_input_files_path = [remote_ue_coordinates_filepath, remote_network_configuration_filepath, remote_resource_configuration_filepath, remote_gnb_positioning_filepath, remote_network_status_filepath, remote_performance_results_filepath]

    response = requests.post('http://{}:{}/datasources/RanSim'.format(utils.configuration['MONITORING_PLATFORM']['monitoring_platform_configuration_ip'], str(utils.configuration['MONITORING_PLATFORM']['monitoring_platform_configuration_port'])), json={
        "metrics": metrics,
        "topic-name": topic_name,
        "remote-output-file-path": remote_output_file_path,
        "remote-input-files-path": remote_input_files_path
    })

    logging.info(type(response.status_code))
    logging.info(response.text)

    return response