#!/usr/bin/env python3

import connexion
import logging

from swagger_server import encoder
import swagger_server.logic.utils as utils


def main():
    utils.init_configuration()
    logging.basicConfig(filename=utils.configuration['LOCAL_PATHS']['local_logfile_path'], encoding='utf-8', level=logging.DEBUG)
    print('Logger configured to write logs to {} file'.format(utils.configuration['LOCAL_PATHS']['local_logfile_path']))
    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(level=logging.DEBUG, filemode='w', filename=utils.configuration['LOCAL_PATHS']['local_logfile_path'], format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
    logging.info('Configuration loaded')
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Orchestrator - RanSimulator Interface'}, pythonic_params=True)
    if utils.configuration['APP']['debug_mode'] == "true":
        app.run(debug=True, use_reloader=True, port=utils.configuration['APP']['listening_port'])
    else:
        app.run(port=utils.configuration['APP']['listening_port'])

if __name__ == '__main__':
    main()
